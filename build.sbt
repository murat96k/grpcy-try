name := "grpcy-try"

version := "0.1"

scalaVersion := "2.12.11"

enablePlugins(AkkaGrpcPlugin)

libraryDependencies ++= Seq(
  "ch.megard" %% "akka-http-cors" % "1.0.0",
  "com.typesafe.akka" %% "akka-stream" % "2.5.27",
  "com.typesafe.akka" %% "akka-http" % "10.1.12",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.12"
)