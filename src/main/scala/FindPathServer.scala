import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import com.typesafe.config.ConfigFactory
import service.FindPathServiceImpl
import traverse.grpc.FindPathServiceHandler

import scala.concurrent.ExecutionContext

//<-- here should be play(run) button
object FindPathServer extends App{
  val conf = ConfigFactory.parseString("akka.http.server.preview.enable-http2 = on") //important to enable http2
    .withFallback(ConfigFactory.defaultApplication())
  val system = ActorSystem("FindPathServer", conf)
  implicit val sys: ActorSystem = system
  implicit val ec: ExecutionContext = sys.dispatcher

  val binding = Http().bindAndHandleAsync(
    FindPathServiceHandler(new FindPathServiceImpl),
    interface = "127.0.0.1",
    port = 8080
  )

  binding.foreach(binding => println("Server started at 8080"))
}
