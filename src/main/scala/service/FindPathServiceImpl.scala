package service

import traverse.grpc.{CandidatePath, FindPathService, TraverseCitiesRequest, TraverseCitiesResponse}
import util.Cities

import scala.collection.mutable
import scala.collection.mutable.{ListBuffer, PriorityQueue}
import scala.concurrent.Future

class FindPathServiceImpl extends FindPathService {

  val adjacencyMatrix = Cities.getAdjacencyMatrix

  //I'm going to traverse through the graph using BFS to find all possible paths to cover needed cities
  //The goal is to visit all the cities. There is no condition to return back to the starting city, so BFS is used. Otherwise it would have been Eulerian cycle and solved differently

  //we need this class to save traversing state: which cities it visited, in which city it is now,
  // how many kilometers were covered now and the path through which it came here
  /**
   *
   * @param visited Bitmask of visited cities
   * @param current index of current city
   * @param cost    how many kilometers needed to come here
   * @param path    cities list which were visited (by order)
   */
  case class State(visited: Int, current: Int, cost: Int, path: List[String])

  //order for priority queue: current shortest path each time will be taken first
  def stateOrder(s: State) = -s.cost

  override def findShortestPath(in: TraverseCitiesRequest): Future[TraverseCitiesResponse] = {
    println("Start searching for shortest path")
    val startingCityIndex: Int = Cities.city2index(in.startingCity)

    //we will use bitmask to see which cities we should cover and which are covered
    //for example, if we need to visit Petropavl and Shymkent: 1000000000000100
    //       - the first 1 corresponds to Petropavl (1 << 17), the penultimate one to Shymkent (1 << 1)
    val citiesToCover = in.citiesList.foldLeft(startingCityIndex)((bitmask, city) => bitmask | 1 << Cities.city2index(city))
    println(citiesToCover.toBinaryString)

    //save states in set in order to avoid repeating
    val setOfStates = mutable.Set[State]()

    //priority queue always will place current shortest path on the top
    //so dequeue() will give us the most effective path for now
    //initialized with startingSity
    val priorityQueue = PriorityQueue(
      State(
        visited = 1 << startingCityIndex,
        current = startingCityIndex,
        cost = 0,
        path = List(in.startingCity)
      )
    )(Ordering.by(stateOrder))

    //it will keep found paths and return them in the end
    val candidateResponses: mutable.ListBuffer[CandidatePath] = new ListBuffer

    while(priorityQueue.nonEmpty) {
      val curr = priorityQueue.dequeue()
      if ((curr.visited & citiesToCover) == citiesToCover) {    //if needed cities are covered, add the found path to candidateResponses
        candidateResponses += CandidatePath(
          totalDistance = curr.cost,
          cities = curr.path.reverse //84-line comment
        )
      }
      else {
        val neighbors = adjacencyMatrix(curr.current) //neighbors of city are in corresponding row (or column, doesn't matter) of adjacencyMatrix

        for (cityIdx <- 0 until Cities.total if (neighbors(cityIdx) != Cities.inf)) { //if distance equals to inf, it means there is no edge between two nodes
          //add to bitmask the next city for the next step state
          val bitmask = curr.visited | (1 << cityIdx)

          //needed for check in the Set.
          //distance is 0 and path is empty, because they don't matter for check
          //matters only visited cities and current city, because if it came to this through other route it would never be better than the previous because of priority queue
          val state = State(bitmask, cityIdx, 0, List.empty)

          if (!setOfStates.contains(state)) {
            priorityQueue += State(
              bitmask,
              cityIdx,
              curr.cost + neighbors(cityIdx), //now the distance will be: past distance + distance to this city from previous
              Cities.allCities(cityIdx) +: curr.path   //use prepend instead of append, cause it works O(1), append works O(n). reverse in the end
            )
            setOfStates += state //add to the set to say, that this route was browsed
          }
        }
      }
    }

    println("Paths found")
    Future.successful(TraverseCitiesResponse(candidateResponses))
  }
}
