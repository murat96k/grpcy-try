package util

import scala.io.Source

object Cities {

  val allCities = Array(
    "Нур-Султан",
    "Алматы",
    "Шымкент",
    "Кокшетау",
    "Актобе",
    "Талдыкорган",
    "Атырау",
    "Усть-Каменогорск",
    "Тараз",
    "Уральск",
    "Караганда",
    "Костанай",
    "Кызылорда",
    "Актау",
    "Павлодар",
    "Петропавловск",
    "Туркестан"
  )
  val total = allCities.size
  val inf = 999999999

  //Нур-Султан  -> 0
  //Алматы      -> 1
  //Шымкент     -> 2
  //Кокшетау    -> 3
  //etc
  val city2index: Map[String, Int] = {
    for (i <- 0 until total) yield {
      (allCities(i) -> i)
    }
  }.toMap


  def getAdjacencyMatrix: Array[Array[Int]] = {
    //initializing adjacency matrix
    //assign with 0 if index corresponds to itself else with infinity
    def fillMatrix(a: Int, b: Int) = if (a == b) 0 else inf
    val adjacencyMatrix = Array.tabulate[Int](total, total)(fillMatrix)

    /*
      Алматы,Талдыкорган,266
      Алматы,Тараз,492
      Тараз,Шымкент,181
      Шымкент,Туркестан,166
      Туркестан,Кызылорда,289
      Кызылорда,Актобе,1056
      Кызылорда,Актау,2246
      Кызылорда,Атырау,1549
      Актобе,Уральск,476
      Актобе,Атырау,631
      Актобе,Актау,1237
      Атырау,Актау,898
      Атырау,Уральск,494
      Актобе,Костанай,775
      Костанай,Нур-Султан,711
      Костанай,Петропавловск,711
      Костанай,Кокшетау,426
      Кокшетау,Нур-Султан,305
      Кокшетау,Петропавловск,178
      Петропавловск,Нур-Султан,503
      Нур-Султан,Павлодар,433
      Нур-Султан,Караганда,261
      Усть-Каменогорск,Талдыкорган,818
      Караганда,Талдыкорган,1161
      Караганда,Алматы,996
    */
    val filename = "src/main/cities.txt"
    val bufferedSource = Source.fromFile(filename)

    for (line <- bufferedSource.getLines) {

      val elements = line.split(',')
      val cities: (Int, Int) = (city2index(elements(0)), city2index(elements(1)))
      val distance = elements(2)

      //filling with values
      adjacencyMatrix(cities._1)(cities._2) = distance.toInt
      adjacencyMatrix(cities._2)(cities._1) = distance.toInt

    }
    bufferedSource.close()

    adjacencyMatrix

  }


}
