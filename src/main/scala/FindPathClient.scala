import akka.actor.ActorSystem
import akka.grpc.GrpcClientSettings
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Route, RouteResult}
import akka.http.scaladsl.{Http2, HttpConnectionContext}
import service.FindPathServiceImpl
import spray.json.DefaultJsonProtocol._
import traverse.grpc.{FindPathService, FindPathServiceClient, FindPathServiceHandler, TraverseCitiesRequest, TraverseCitiesResponse}
import util.Cities

import scala.concurrent.Future

//<-- here should be play(run) button
object FindPathClient extends App {
  implicit val system = ActorSystem("FindPathSClient")
  implicit val ec = system.dispatcher

  //connecting to the server on 8080
  val clientSettings = GrpcClientSettings.connectToServiceAt("127.0.0.1", 8080).withTls(false)
  val client: FindPathService = FindPathServiceClient(clientSettings)

  //routing
  val findPathRoute: Route = path("calculate-path") {
    concat(

      //endpoint for visiting all of the cities
      // GET /calculate-path?startingCity=Almaty
      get {
        parameter('startingCity) { startingCity =>
          println("Performing request")

          val reply = client.findShortestPath(
            TraverseCitiesRequest(
              startingCity = startingCity,
              citiesList = Cities.allCities
            )
          )

          completeJob(reply)
        }
      },

      //give in the body which cities to visit
      //can give more cities in the output, because there is no direct road between some cities
      /*
      POST /calculate-path
      BODY: json array of strings
          [
              "Алматы",
              "Нур-Султан",
              "Петропавловск"
          ]
      */
      post {
        parameter('startingCity) { startingCity =>
          entity(as[List[String]]) { entity =>
            println("Performing request")

            val reply = client.findShortestPath(
              TraverseCitiesRequest(
                startingCity = startingCity,
                citiesList = entity
              )
            )

            completeJob(reply)
          }
        }
      }
    )
  }

  def completeJob(reply: Future[TraverseCitiesResponse]) =
    onSuccess(reply) {
      case value: TraverseCitiesResponse =>
        val output = for (p <- value.path) yield s"${p.totalDistance} километров: ${p.cities.mkString(", ")}"
        complete(output)
      case _ =>
        complete("Error")
    }

  //create binding to 8082-port for receiving http-requests
  val binding = Http2().bindAndHandleAsync(
    Route.asyncHandler(findPathRoute),
    interface = "127.0.0.1",
    port = 8082,
    connectionContext = HttpConnectionContext()
  )

  binding.foreach(binding => println("Client bound to 8082"))

}
