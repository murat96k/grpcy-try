// Generated by the Scala Plugin for the Protocol Buffer Compiler.
// Do not edit!
//
// Protofile syntax: PROTO3

package traverse.grpc

@SerialVersionUID(0L)
final case class CandidatePath(
    totalDistance: _root_.scala.Int = 0,
    cities: _root_.scala.Seq[_root_.scala.Predef.String] = _root_.scala.Seq.empty,
    unknownFields: _root_.scalapb.UnknownFieldSet = _root_.scalapb.UnknownFieldSet.empty
    ) extends scalapb.GeneratedMessage with scalapb.lenses.Updatable[CandidatePath] {
    @transient
    private[this] var __serializedSizeCachedValue: _root_.scala.Int = 0
    private[this] def __computeSerializedValue(): _root_.scala.Int = {
      var __size = 0
      
      {
        val __value = totalDistance
        if (__value != 0) {
          __size += _root_.com.google.protobuf.CodedOutputStream.computeInt32Size(1, __value)
        }
      };
      cities.foreach { __item =>
        val __value = __item
        __size += _root_.com.google.protobuf.CodedOutputStream.computeStringSize(2, __value)
      }
      __size += unknownFields.serializedSize
      __size
    }
    override def serializedSize: _root_.scala.Int = {
      var read = __serializedSizeCachedValue
      if (read == 0) {
        read = __computeSerializedValue()
        __serializedSizeCachedValue = read
      }
      read
    }
    def writeTo(`_output__`: _root_.com.google.protobuf.CodedOutputStream): _root_.scala.Unit = {
      {
        val __v = totalDistance
        if (__v != 0) {
          _output__.writeInt32(1, __v)
        }
      };
      cities.foreach { __v =>
        val __m = __v
        _output__.writeString(2, __m)
      };
      unknownFields.writeTo(_output__)
    }
    def withTotalDistance(__v: _root_.scala.Int): CandidatePath = copy(totalDistance = __v)
    def clearCities = copy(cities = _root_.scala.Seq.empty)
    def addCities(__vs: _root_.scala.Predef.String*): CandidatePath = addAllCities(__vs)
    def addAllCities(__vs: Iterable[_root_.scala.Predef.String]): CandidatePath = copy(cities = cities ++ __vs)
    def withCities(__v: _root_.scala.Seq[_root_.scala.Predef.String]): CandidatePath = copy(cities = __v)
    def withUnknownFields(__v: _root_.scalapb.UnknownFieldSet) = copy(unknownFields = __v)
    def discardUnknownFields = copy(unknownFields = _root_.scalapb.UnknownFieldSet.empty)
    def getFieldByNumber(__fieldNumber: _root_.scala.Int): _root_.scala.Any = {
      (__fieldNumber: @_root_.scala.unchecked) match {
        case 1 => {
          val __t = totalDistance
          if (__t != 0) __t else null
        }
        case 2 => cities
      }
    }
    def getField(__field: _root_.scalapb.descriptors.FieldDescriptor): _root_.scalapb.descriptors.PValue = {
      _root_.scala.Predef.require(__field.containingMessage eq companion.scalaDescriptor)
      (__field.number: @_root_.scala.unchecked) match {
        case 1 => _root_.scalapb.descriptors.PInt(totalDistance)
        case 2 => _root_.scalapb.descriptors.PRepeated(cities.iterator.map(_root_.scalapb.descriptors.PString).toVector)
      }
    }
    def toProtoString: _root_.scala.Predef.String = _root_.scalapb.TextFormat.printToUnicodeString(this)
    def companion = traverse.grpc.CandidatePath
}

object CandidatePath extends scalapb.GeneratedMessageCompanion[traverse.grpc.CandidatePath] {
  implicit def messageCompanion: scalapb.GeneratedMessageCompanion[traverse.grpc.CandidatePath] = this
  def merge(`_message__`: traverse.grpc.CandidatePath, `_input__`: _root_.com.google.protobuf.CodedInputStream): traverse.grpc.CandidatePath = {
    var __totalDistance = `_message__`.totalDistance
    val __cities = (_root_.scala.collection.immutable.Vector.newBuilder[_root_.scala.Predef.String] ++= `_message__`.cities)
    var `_unknownFields__`: _root_.scalapb.UnknownFieldSet.Builder = null
    var _done__ = false
    while (!_done__) {
      val _tag__ = _input__.readTag()
      _tag__ match {
        case 0 => _done__ = true
        case 8 =>
          __totalDistance = _input__.readInt32()
        case 18 =>
          __cities += _input__.readStringRequireUtf8()
        case tag =>
          if (_unknownFields__ == null) {
            _unknownFields__ = new _root_.scalapb.UnknownFieldSet.Builder(_message__.unknownFields)
          }
          _unknownFields__.parseField(tag, _input__)
      }
    }
    traverse.grpc.CandidatePath(
        totalDistance = __totalDistance,
        cities = __cities.result(),
        unknownFields = if (_unknownFields__ == null) _message__.unknownFields else _unknownFields__.result()
    )
  }
  implicit def messageReads: _root_.scalapb.descriptors.Reads[traverse.grpc.CandidatePath] = _root_.scalapb.descriptors.Reads{
    case _root_.scalapb.descriptors.PMessage(__fieldsMap) =>
      _root_.scala.Predef.require(__fieldsMap.keys.forall(_.containingMessage == scalaDescriptor), "FieldDescriptor does not match message type.")
      traverse.grpc.CandidatePath(
        totalDistance = __fieldsMap.get(scalaDescriptor.findFieldByNumber(1).get).map(_.as[_root_.scala.Int]).getOrElse(0),
        cities = __fieldsMap.get(scalaDescriptor.findFieldByNumber(2).get).map(_.as[_root_.scala.Seq[_root_.scala.Predef.String]]).getOrElse(_root_.scala.Seq.empty)
      )
    case _ => throw new RuntimeException("Expected PMessage")
  }
  def javaDescriptor: _root_.com.google.protobuf.Descriptors.Descriptor = CitiesProto.javaDescriptor.getMessageTypes.get(1)
  def scalaDescriptor: _root_.scalapb.descriptors.Descriptor = CitiesProto.scalaDescriptor.messages(1)
  def messageCompanionForFieldNumber(__number: _root_.scala.Int): _root_.scalapb.GeneratedMessageCompanion[_] = throw new MatchError(__number)
  lazy val nestedMessagesCompanions: Seq[_root_.scalapb.GeneratedMessageCompanion[_ <: _root_.scalapb.GeneratedMessage]] = Seq.empty
  def enumCompanionForFieldNumber(__fieldNumber: _root_.scala.Int): _root_.scalapb.GeneratedEnumCompanion[_] = throw new MatchError(__fieldNumber)
  lazy val defaultInstance = traverse.grpc.CandidatePath(
    totalDistance = 0,
    cities = _root_.scala.Seq.empty
  )
  implicit class CandidatePathLens[UpperPB](_l: _root_.scalapb.lenses.Lens[UpperPB, traverse.grpc.CandidatePath]) extends _root_.scalapb.lenses.ObjectLens[UpperPB, traverse.grpc.CandidatePath](_l) {
    def totalDistance: _root_.scalapb.lenses.Lens[UpperPB, _root_.scala.Int] = field(_.totalDistance)((c_, f_) => c_.copy(totalDistance = f_))
    def cities: _root_.scalapb.lenses.Lens[UpperPB, _root_.scala.Seq[_root_.scala.Predef.String]] = field(_.cities)((c_, f_) => c_.copy(cities = f_))
  }
  final val TOTALDISTANCE_FIELD_NUMBER = 1
  final val CITIES_FIELD_NUMBER = 2
  def of(
    totalDistance: _root_.scala.Int,
    cities: _root_.scala.Seq[_root_.scala.Predef.String]
  ): _root_.traverse.grpc.CandidatePath = _root_.traverse.grpc.CandidatePath(
    totalDistance,
    cities
  )
  // @@protoc_insertion_point(GeneratedMessageCompanion[CandidatePath])
}
