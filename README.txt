Service for finding shortest path to visit given cities.
I created two endpoints:
    GET http://127.0.0.1:8082/calculate-path?startingCity=Алматы
            Visits all of 17 cities of Kazakhstan
            Returns all possible paths if increasing order

    POST http://127.0.0.1:8082/calculate-path?startingCity=Алматы
        BODY: [
                 "Алматы",
                 "Нур-Султан",
                 "Петропавловск"
             ]

            Visits cities which are given in the body
            Number of cities in the output can be more than in the input. It is because of there is no direct road between some cities



Run first src/main/scala/FindPathServer.scala
Then run src/main/scala/FindPathClient.scala

Endpoints can be accessed through Postman.
You can try GET endpoint with your browser (Firefox displays it better)

curls:

    curl --location --request GET 'http://127.0.0.1:8082/calculate-path?startingCity=%D0%90%D0%BB%D0%BC%D0%B0%D1%82%D1%8B'

    curl --location --request POST 'http://127.0.0.1:8082/calculate-path?startingCity=%D0%90%D0%BB%D0%BC%D0%B0%D1%82%D1%8B' \
    --header 'Content-Type: application/json' \
    --data-raw '[
        "Алматы",
        "Нур-Султан",
        "Петропавловск"
    ]'